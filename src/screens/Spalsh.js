import {View, Image} from 'react-native';
import React from 'react';
import logo from '../assets/react.png';

const Spalsh = () => {
  return (
    <View
      style={{alignItems: 'center', justifyContent: 'center', height: '100%'}}>
      <Image source={logo} style={{width: 200, height: 200}} />
    </View>
  );
};

export default Spalsh;
