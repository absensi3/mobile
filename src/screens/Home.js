import {
  View,
  Text,
  TouchableOpacity,
  ToastAndroid,
  PermissionsAndroid,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import pegawaiApi from '../api/pegawai';
import absensiApi from '../api/absensi';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
// import Geolocation from '@react-native-community/geolocation';
import Geolocation from 'react-native-geolocation-service';
import RNMockLocationDetector from 'react-native-mock-location-detector';

const Home = () => {
  const insets = useSafeAreaInsets();
  const [permissionGranted, setPermissionGranted] = useState(false);
  const [position, setPosition] = useState({lat: 0, long: 0});
  const [watchId, setWatchId] = useState(null);
  const [pegawai, setPegawai] = useState({
    nip: '',
    nama: '',
    gender: '',
    t_lahir: '',
    tgl_lahir: '',
    alamat: '',
    no_telp: '',
    email: '',
    satgas: '',
  });
  const loadData = async () => {
    try {
      const res = await pegawaiApi.get();
      if (res.status === 200) {
        console.log(res.data.data);
        setPegawai(res.data.data);
      }
    } catch (err) {
      console.log(err);
    }
  };
  // useEffect(() => {
  //   console.log('watch');
  //   if (permissionGranted) {
  //     console.log('permissionGranted', permissionGranted);
  //     const id = Geolocation.watchPosition(
  //       pos => {
  //         console.log(pos);
  //         setPosition({
  //           lat: pos.coords.latitude,
  //           long: pos.coords.longitude,
  //         });
  //       },
  //       err => {
  //         console.log(err);
  //       },
  //       {
  //         enableHighAccuracy: true,
  //         accuracy: {android: 'high'},
  //         distanceFilter: 1,
  //       },
  //     );
  //     setWatchId(id);
  //   }
  // }, [permissionGranted]);
  // useEffect(() => {
  //   console.log('watchID', watchId);
  //   return () => {
  //     Geolocation.clearWatch(watchId);
  //   };
  // }, [watchId]);
  useEffect(() => {
    requestPermission();
  }, []);
  useEffect(() => {
    loadData();
  }, []);
  const postAbsen = async data => {
    try {
      const res = await absensiApi.absen(data);
      if (res.status === 200) {
        console.log(res.data.data);
        ToastAndroid.showWithGravity(
          res.data.msg,
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
      }
    } catch (err) {
      ToastAndroid.showWithGravity(
        err.response.data.msg,
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
    }
  };
  const requestPermission = async () => {
    try {
      const grant = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Meminta data lokasi',
          message: 'Absensi ingin mengakses data lokasi anda. Izinkan?',
          buttonNeutral: 'Tanya nanti',
          buttonPositive: 'Izinkan',
          buttonNegative: 'Tolak',
        },
      );
      setPermissionGranted(grant === PermissionsAndroid.RESULTS.GRANTED);
    } catch (err) {
      console.log(err);
      ToastAndroid.showWithGravity(
        'Akses lokasi tidak diberikan',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
    }
  };
  const absen = async () => {
    try {
      if (permissionGranted) {
        const isLocationMocked =
          await RNMockLocationDetector.checkMockLocationProvider();
        if (isLocationMocked) {
          ToastAndroid.showWithGravity(
            'Mohon disable mock lokasi terlebih dahulu',
            ToastAndroid.SHORT,
            ToastAndroid.BOTTOM,
          );
        } else {
          // await postAbsen(position);
          Geolocation.getCurrentPosition(
            async pos => {
              console.log(pos);
              if (pos.mocked) {
                ToastAndroid.showWithGravity(
                  'Mohon disable mock lokasi terlebih dahulu',
                  ToastAndroid.SHORT,
                  ToastAndroid.BOTTOM,
                );
              } else {
                const data = {
                  lat: pos.coords.latitude,
                  long: pos.coords.longitude,
                };
                await postAbsen(data);
              }
            },
            err => {
              console.log(err);
            },
            {enableHighAccuracy: true, timeout: 1000, maximumAge: 3000},
          );
        }
      } else {
        ToastAndroid.showWithGravity(
          'Akses lokasi tidak diberikan',
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
      }
    } catch (err) {
      console.log(err);
      ToastAndroid.showWithGravity(
        err.response.data.msg,
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
    }
  };
  return (
    <View
      style={{
        // padding: 10,
        paddingTop: insets.top,
        paddingBottom: insets.bottom,
        paddingRight: insets.right + 10,
        paddingLeft: insets.left + 10,
        height: '100%',
        justifyContent: 'center',
      }}>
      <View style={{padding: 10, backgroundColor: 'white', borderRadius: 10}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={{width: 100}}>NIP</Text>
          <Text style={{width: 10}}>:</Text>
          <Text>{pegawai.nip}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={{width: 100}}>Nama</Text>
          <Text style={{width: 10}}>:</Text>
          <Text>{pegawai.nama}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={{width: 100}}>Gender</Text>
          <Text style={{width: 10}}>:</Text>
          <Text>{pegawai.gender}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={{width: 100}}>Tempat Lahir</Text>
          <Text style={{width: 10}}>:</Text>
          <Text>{pegawai.t_lahir}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={{width: 100}}>Tanggal Lahir</Text>
          <Text style={{width: 10}}>:</Text>
          <Text>{pegawai.tgl_lahir}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={{width: 100}}>Alamat</Text>
          <Text style={{width: 10}}>:</Text>
          <Text>{pegawai.alamat}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={{width: 100}}>Nomor Telpon</Text>
          <Text style={{width: 10}}>:</Text>
          <Text>{pegawai.no_telp}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={{width: 100}}>Email</Text>
          <Text style={{width: 10}}>:</Text>
          <Text>{pegawai.email}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={{width: 100}}>Satgas</Text>
          <Text style={{width: 10}}>:</Text>
          <Text>{pegawai.satgas}</Text>
        </View>
      </View>
      <TouchableOpacity
        style={{backgroundColor: '#42adf5', borderRadius: 10, marginTop: 10}}
        onPress={absen}>
        <Text
          style={{
            textAlign: 'center',
            textAlignVertical: 'center',
            height: 50,
          }}>
          Absen
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Home;
