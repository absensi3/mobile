import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import React, {useState} from 'react';
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
import * as yup from 'yup';
import Input from '../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome5';
import authApi from '../api/auth';
import {useDispatch} from 'react-redux';
import {loginAction} from '../store/pegawai';

const Login = ({navigation}) => {
  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    mode: 'all',
    resolver: yupResolver(
      yup.object().shape({
        nip: yup
          .string()
          .required('NIP tidak boleh kosong')
          .test('', 'NIP hanya boleh menggunakan karakter angka(0-9)', value =>
            /^\d+$/.test(value),
          )
          .length(18, 'NIP harus 18 karakter'),
        password: yup
          .string()
          .required('Password tidak boleh kosong')
          .min(8, 'Password minimal 8 karakter'),
      }),
    ),
    defaultValues: {
      nip: '234567890987654321',
      password: '12345678',
    },
  });
  const dispatch = useDispatch();
  const [showPass, setShowPass] = useState(false);
  const login = async data => {
    try {
      const res = await authApi.login(data);
      if (res.status === 200) {
        dispatch(loginAction(res.data.data));
        navigation.navigate('HomeTabs');
      }
    } catch (err) {
      console.log(err);
      ToastAndroid.showWithGravity(
        err.response.data.msg,
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
    }
  };
  return (
    <View style={loginStyle.wrapper}>
      <Icon name="user-circle" solid size={100} color="black" />
      <Input
        control={control}
        name="nip"
        label="NIP"
        errors={errors.nip}
        placeholder="NIP"
        keyboardType="numeric"
        prefix={
          <View style={loginStyle.sideFix}>
            <Icon
              name="user"
              color={errors.nip ? 'red' : 'black'}
              size={20}
              solid
            />
          </View>
        }
      />
      <Input
        control={control}
        name="password"
        label="Password"
        placeholder="Password"
        secureTextEntry={!showPass}
        prefix={
          <View style={loginStyle.sideFix}>
            <Icon
              name="lock"
              color={errors.password ? 'red' : 'black'}
              size={20}
              solid
            />
          </View>
        }
        suffix={
          <TouchableOpacity
            style={loginStyle.sideFix}
            onPress={() => setShowPass(!showPass)}>
            {showPass ? (
              <Icon
                name="eye-slash"
                color={errors.password ? 'red' : 'black'}
                size={20}
                solid
              />
            ) : (
              <Icon
                name="eye"
                color={errors.password ? 'red' : 'black'}
                size={20}
                solid
              />
            )}
          </TouchableOpacity>
        }
      />
      <TouchableOpacity
        onPress={handleSubmit(login)}
        style={loginStyle.buttonWrapper}>
        <Text style={loginStyle.button}>Login</Text>
      </TouchableOpacity>
    </View>
  );
};

const loginStyle = StyleSheet.create({
  wrapper: {
    alignItems: 'center',
    marginTop: 100,
  },
  sideFix: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 40,
  },
  buttonWrapper: {
    width: '100%',
    paddingHorizontal: 10,
    marginTop: 10,
  },
  button: {
    backgroundColor: '#42adf5',
    width: '100%',
    height: 50,
    borderRadius: 10,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 15,
    fontWeight: 'bold',
  },
});

export default Login;
