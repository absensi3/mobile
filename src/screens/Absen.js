import {View, Text} from 'react-native';
import absensiApi from '../api/absensi';
import React, {useEffect, useState} from 'react';

const Absen = ({navigation}) => {
  const [absensi, setAbsensi] = useState({
    absensi: [],
  });
  const loadData = async () => {
    try {
      const res = await absensiApi.getAll();
      if (res.status === 200) {
        console.log(res.data.data);
        setAbsensi(res.data.data);
      }
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    loadData();
  }, [navigation]);
  return (
    <View>
      <Text>Absen</Text>
      {absensi.absensi.map((a, idx) => (
        <Text key={idx}>{a.created_at}</Text>
      ))}
    </View>
  );
};

export default Absen;
