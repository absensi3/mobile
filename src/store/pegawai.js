import {createSlice} from '@reduxjs/toolkit';

const pegawaiSlice = createSlice({
  name: 'pegawai',
  initialState: {
    accessToken: '',
    pegawai: '',
  },
  reducers: {
    loginAction: (pegawai, action) => {
      pegawai.accessToken = action.payload.access_token;
      pegawai.pegawai = action.payload.pegawai;
    },
  },
});

export const {loginAction} = pegawaiSlice.actions;
export default pegawaiSlice.reducer;
