import {configureStore} from '@reduxjs/toolkit';
import pegawaiReducer from './pegawai';

const store = configureStore({
  reducer: {
    pegawai: pegawaiReducer,
  },
});

export default store;
