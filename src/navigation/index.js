import React, {useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Spalsh from '../screens/Spalsh';
import {useSelector} from 'react-redux';
import Home from '../screens/Home';
import Login from '../screens/Login';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Absen from '../screens/Absen';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';

const Stack = createNativeStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const HomeTabs = () => {
  return (
    <Tab.Navigator
      barStyle={{
        marginHorizontal: 10,
        marginBottom: 10,
        padding: 7,
        borderRadius: 20,
        backgroundColor: 'deepskyblue',
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = 'home';
          } else {
            iconName = 'clipboard-list';
          }

          return <Icon name={iconName} brand={focused} color={color} />;
        },
        tabBarActiveTintColor: 'white',
        tabBarInactiveTintColor: 'grey',
        tabBarLabelStyle: {fontSize: 12},
      })}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Absen" component={Absen} />
    </Tab.Navigator>
  );
};

const Navigation = () => {
  const [loading, setLoading] = useState(true);
  const token = useSelector(state => state.pegawai.accessToken);
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 3000);
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        {loading && (
          <Stack.Screen
            name="Splash"
            component={Spalsh}
            options={{headerShown: false}}
          />
        )}
        {token ? (
          <Stack.Screen name="HomeTabs" component={HomeTabs} />
        ) : (
          <Stack.Screen name="Login" component={Login} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
