import api from '.';

const pegawaiApi = {
  get: () => {
    return api.get('/mobile/pegawai');
  },
};

export default pegawaiApi;
