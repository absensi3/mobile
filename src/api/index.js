import axios from 'axios';
import store from '../store';

const api = axios.create({
  baseURL: 'http://192.168.1.108:8000',
  // baseURL: 'http://192.168.0.109:8000',
  // baseURL: 'http://192.168.43.22:8000',
});

api.interceptors.request.use(req => {
  if (!req.url.includes('/login')) {
    req.headers = {
      Authorization: `Bearer ${store.getState().pegawai.accessToken}`,
    };
  }
  return req;
});

export default api;
