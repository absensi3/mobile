import api from '.';

const authApi = {
  login: data => {
    return api.post('/auth/pegawai/login', data);
  },
};

export default authApi;
