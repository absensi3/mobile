import api from '.';

const absensiApi = {
  absen: data => {
    return api.post('/mobile/absensi', data);
  },
  getAll: () => {
    return api.get('/mobile/absensi');
  },
};

export default absensiApi;
