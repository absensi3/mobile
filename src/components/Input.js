import React, {useRef} from 'react';
import {Controller} from 'react-hook-form';
import {Animated, StyleSheet, Text, TextInput, View} from 'react-native';

function Input({
  control,
  name,
  errors,
  label,
  prefix,
  suffix,
  style,
  ...props
}) {
  const input = useRef();
  const floatingTop = useRef(new Animated.Value(17)).current;
  const floatingSize = useRef(new Animated.Value(14)).current;
  const floatingOpacity = useRef(new Animated.Value(0)).current;
  const floatingLabel = () => {
    Animated.timing(floatingTop, {
      toValue: -8,
      duration: 300,
      useNativeDriver: false,
    }).start();
    Animated.timing(floatingSize, {
      toValue: 13,
      duration: 300,
      useNativeDriver: false,
    }).start();
    Animated.timing(floatingOpacity, {
      toValue: 1,
      duration: 300,
      useNativeDriver: false,
    }).start();
  };
  const fallLabel = () => {
    Animated.timing(floatingTop, {
      toValue: 15,
      duration: 300,
      useNativeDriver: false,
    }).start();
    Animated.timing(floatingSize, {
      toValue: 14,
      duration: 300,
      useNativeDriver: false,
    }).start();
    Animated.timing(floatingOpacity, {
      toValue: 0,
      duration: 300,
      useNativeDriver: false,
    }).start();
  };
  return (
    <Controller
      control={control}
      name={name}
      render={({field: {onChange, onBlur, value, n}, fieldState: {error}}) => {
        return (
          <>
            <View style={[inputStyle.wrapper, style]}>
              <View style={inputStyle.inputWrapper}>
                {prefix && (
                  <View style={inputStyle.prefix(error)}>{prefix}</View>
                )}
                <TextInput
                  ref={input}
                  onChangeText={e => {
                    onChange(e);
                  }}
                  onBlur={e => {
                    onBlur(e);
                    if (!value) {
                      fallLabel();
                    }
                  }}
                  onFocus={e => {
                    floatingLabel();
                  }}
                  value={value}
                  style={inputStyle.input(error, prefix, suffix)}
                  // secureTextEntry={secureTextEntry}
                  {...props}
                />
                <Animated.Text
                  onPress={() => input.current.focus()}
                  style={inputStyle.floatingLabel(
                    floatingTop,
                    floatingSize,
                    floatingOpacity,
                    prefix,
                    error,
                  )}>
                  {label}
                </Animated.Text>
                {suffix && (
                  <View style={inputStyle.suffix(error)}>{suffix}</View>
                )}
              </View>
              {error && <Text style={inputStyle.error}>{error.message}</Text>}
            </View>
          </>
        );
      }}
    />
  );
}

const inputStyle = StyleSheet.create({
  wrapper: {
    padding: 10,
    width: '100%',
    // backgroundColor: 'white',
    backgroundColor: '#f2f2f2',
  },
  inputWrapper: {
    flexDirection: 'row',
  },
  floatingLabel: (top, size, opacity, prefix, error) => {
    return {
      paddingHorizontal: 2,
      opacity: opacity,
      // backgroundColor: 'white',
      backgroundColor: '#f2f2f2',
      color: error ? 'red' : 'black',
      position: 'absolute',
      top: top,
      left: prefix ? 60 : 10,
      fontSize: size,
    };
  },
  prefix: error => {
    return {
      aligntItems: 'center',
      justifyContent: 'center',
      borderTopLeftRadius: 10,
      borderBottomLeftRadius: 10,
      borderWidth: 1,
      borderRightWidth: 0,
      borderColor: error ? 'red' : 'black',
      padding: 5,
    };
  },
  input: (error, prefix, suffix) => {
    return {
      flex: 1,
      color: error ? 'red' : 'black',
      paddingHorizontal: 10,
      borderTopLeftRadius: prefix ? 0 : 10,
      borderBottomLeftRadius: prefix ? 0 : 10,
      borderTopRightRadius: suffix ? 0 : 10,
      borderBottomRightRadius: suffix ? 0 : 10,
      borderWidth: 1,
      borderColor: error ? 'red' : 'black',
    };
  },
  suffix: error => {
    return {
      aligntItems: 'center',
      justifyContent: 'center',
      borderTopRightRadius: 10,
      borderBottomRightRadius: 10,
      borderWidth: 1,
      borderLeftWidth: 0,
      borderColor: error ? 'red' : 'black',
      padding: 5,
    };
  },
  error: {
    color: 'red',
    fontSize: 13,
    paddingLeft: 10,
  },
});

export default Input;
